jQuery(document).ready(function () {

    if(jQuery('#myNL2GoForm').length > 0) {
        var values = document.cookie.split(';')
        var name = "nlTeaserMail=";
        for (var i = 0; i < values.length; i++) {
            var c = values[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                var nlMail = c.substring(name.length, c.length);
                jQuery('#fhandler-email').val(nlMail);
            }
        }

        // validation for apply now
        jQuery("#nlsubscr").validate({
            // jump to missing field
            focusInvalid: false,
            invalidHandler: function (form, validator) {
                if (!validator.numberOfInvalids())
                    return;
                jQuery('html, body').animate({
                    scrollTop: jQuery(validator.errorList[0].element).offset().top - 150
                }, 1000);
            },
            rules: {
                'country': {
                    required: true,
                },
                'email': {
                    required: true,
                    email: true
                },
                'language': {
                    required: true,
                }
            },
            messages: {
                'country': {
                    required: "Please select an option from the list!"
                },
                'email': {
                    required: "Please add a valid email!"
                },
                'language': {
                    required: "Please select a language!"
                },
            }
        });
        var myPath = window.location.pathname.split( '/' );
        if( myPath[1] == 'de'){
            // DYNAMICALLY over-ride error messages for translation
            jQuery('[name="country"]').rules('add', {
                messages: {
                    required: "Bitte geben Sie Ihr Land an."
                }
            });
            jQuery('[name="email"]').rules('add', {
                messages: {
                    required: "Bitte geben Sie Ihre Email-Adresse an."
                }
            });
            jQuery('#fhandler-language-1').rules('add', {
                messages: {
                    required: "Bitte geben Sie eine Sprache an."
                }
            });
            jQuery('#fhandler-language-2').rules('add', {
                messages: {
                    required: "Bitte geben Sie eine Sprache an."
                }
            });
        }

    }

    // no Label on NL2Go-formular unless you click the field
    jQuery('#myNL2GoForm #nlsubscr .form-control').focus(function() {
        var thisInput = jQuery(this);

        // clear the placeholder and show the label
        thisInput.attr('placeholder', "");
        thisInput.parent().parent().find('.hideMe').css("color", "#222222");

        // change input field? show the placeholder and hide label if the input is empty
        thisInput.focusout(function() {
            if( thisInput.val() == "" ) {
                thisInput.attr('placeholder', thisInput.parent().parent().find('.hideMe').text().trim().replace(/\s+/g, '') );
                thisInput.parent().parent().find('.hideMe').css("color", "#FFFFFF");
            }
        });
    })


});