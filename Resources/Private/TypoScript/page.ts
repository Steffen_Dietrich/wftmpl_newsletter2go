page {

    includeCSS {
    }

    includeJSFooter {
        validateJS = EXT:wftmpl_newsletter2go/Resources/Public/Js/jquery.validation.min.js
        customNL2GoJS = EXT:wftmpl_newsletter2go/Resources/Public/Js/customNL2Go.js
    }

}

