<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Newsletter2GoAPI');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
		'Webfox.' . $_EXTKEY,
		'List',
		'Newsletter2GoAPI'
);