<?php
namespace Webfox\WftmplNewsletter2go\Domain\Repository;

use Webfox\WftmplNewsletter2go\Domain\Repository\Newsletter2Go_REST_Api;
use \TYPO3\CMS\Extbase\Persistence\Repository;

class DOIRepository extends Repository {

	/**
	 *
	 * @param string $gender
	 * @param string $firstname
	 * @param string $lastname
	 * @param string $email
	 * @param string $company
	 * @param string $country
	 * @param string $language
	 */
	public function createNewUser($gender = '', $firstname = '', $lastname = '', $email = '', $company = '', $country = '', $language = '') {

		$gender = filter_var($_POST['gender'], FILTER_SANITIZE_STRING);
		$firstname = filter_var($_POST['firstname'], FILTER_SANITIZE_STRING);
		$lastname = filter_var($_POST['lastname'], FILTER_SANITIZE_STRING);
		$email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
		$company = filter_var($_POST['company'], FILTER_SANITIZE_STRING);
		$country = filter_var($_POST['country'], FILTER_SANITIZE_STRING);
		$language = filter_var($_POST['language'], FILTER_SANITIZE_STRING);

		// Template -> Setup. Needs refactoring but works for now
		$useLiveData = 			$GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_wftmpl_newsletter2go.']['settings.']['useLiveData'];
		$afterRegistration = 	$GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_wftmpl_newsletter2go.']['settings.']['registration'];
		$alreadyRegistered = 	$GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_wftmpl_newsletter2go.']['settings.']['alreadyregistered'];
		$afterRegistrationDE = 	$GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_wftmpl_newsletter2go.']['settings.']['registrationDE'];
		$alreadyRegisteredDE = 	$GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_wftmpl_newsletter2go.']['settings.']['alreadyregisteredDE'];


		// TEST ACCOUNT
		$authKey = "hybbrylf_utK0m4_LO0Tsb_vVx8MlAs_ydWqDnB4I7:d5po9v83";
		$userEmail = "sysmessages@agentur-webfox.de";
		$userPassword = "s37plKIR257";
		$lid = "kl1tiyyf";
		$endPointEN = "d5po9v83-8tub83ev-82i";
		$endPointDE = "d5po9v83-8tub83ev-82i";
		/* to get the endpoint
		 * https://ui.newsletter2go.com/forms
		 * pick form (or create new one if needed)
		 * "Formular einbetten"
		 * search the snippet for something like this: d5po9v83-8tub83ev-82i
		 */


		// MYCLIMATE LIVE
		if($useLiveData !== NULL) {
			$authKey = "ycaoja6r_LdwDou_tGxhTJ_xDABY7E2_a0rcddeEby:nufo9c9v";
			$userEmail = "newsmail@myclimate.org";
			$userPassword = "!Newsle11er!";
			$lid = "bmf9jsaf";
			$endPointEN = "nufo9c9v-imzpxlyv-x8h";
			$endPointDE = "nufo9c9v-tcq9gc3n-1aux";
		}
		// API TEST GRUPPE AUF LIVE
		//$gid = "m886fwxj";

		$api = new Newsletter2Go_REST_Api($authKey, $userEmail, $userPassword);
		$api->setSSLVerification(false);

		// Set user data for NL2Go
		$doubleOptInData = array("recipient" => array(
			"gender" =>     $gender,
			"first_name" => $firstname,
			"last_name" =>  $lastname,
			"email" => 		$email,
			"Firma" => 		$company,
			"Land" => 	 	$country,
			"Sprache" =>	$language)
		);


		// check if new user or not.
		$checkUserFilter = array(
			"_filter" => 'email=="'.$email.'"',
			"_limit" => "1",
			"_offset" => "0"
		);
		$checkUserUpdateEndpoint = "/lists/$lid/recipients";
		$checkUser = $api->curl($checkUserUpdateEndpoint, $checkUserFilter, "GET");

		// If new user do the DOI. If not send to "already registered"
		if($checkUser->info->count == 0) {

			// EN as default
			$doubleOptInEndpoint = "/forms/submit/".$endPointEN;
			if ($language == 'Deutsch') $doubleOptInEndpoint = "/forms/submit/".$endPointDE;

			$createUpdateUser = $api->curl($doubleOptInEndpoint, $doubleOptInData, "POST");

			// old stuff, only this worked on old system...needs to be refactored
			switch ($language) {
				case "English":
				case "Englisch":
					header("Location: $afterRegistration");
					break;
				case "German":
				case "Deutsch":
				header("Location: $afterRegistrationDE");
					break;
				default :
					header("Location: $afterRegistration");
					break;
			}
		} else {
			switch ($language) {
				case "English":
				case "Englisch":
					header("Location: $alreadyRegistered");
					break;
				case "German":
				case "Deutsch":
					header("Location: $alreadyRegisteredDE");
					break;
				default :
					header("Location: $alreadyRegistered");
					break;
			}
		}



		/* only > PHP 5.3 - old system stops at 5.2 ^^ Refactor on update
		$recipient_data =  [
			//"_limit" => 1,
			//"_expand" => "false",
			//"_fields" => ["Sprache", "Firma", "Land"]
		];

		//$recipient_data = ["Land" => "Algeria", "email" => "dietrich@agentur-webfox.de"];

		//$lists = $api->curl($endpoint, $recipient_data, "POST");
		//	echo "<pre>";
		//	var_dump( $lists );
		//	echo "</pre>";

		// Land_alt=like="%Test%"
		*/


	}

}