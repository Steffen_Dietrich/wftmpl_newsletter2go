<?php 

namespace Webfox\WftmplNewsletter2go\Controller;
use \TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \Webfox\WftmplNewsletter2go\Domain\Model\Repository\DOIRepository;

class Newsletter2GoAPIController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
	/**
	 * doiRepository
	 *
	 * @var \Webfox\WftmplNewsletter2go\Domain\Repository\DOIRepository
	 * @inject
	 */
	protected $doiRepository = null;
	
	/**
	 * action list
	 * @return void
	 */
	public function listAction() {

	}

	/**
	 * action new
	 * @return void
	 */
	public function newAction(){
		$newUser = $this->doiRepository->createNewUser();
	}
	
}

?>