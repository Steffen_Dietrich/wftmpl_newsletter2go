<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "wftmpl_newsletter2go"
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Newsletter2GoAPI',
	'description' => 'Newsletter2Go API Anbindung',
	'category' => 'plugin',
	'author' => 'Steffen Dietrich',
	'author_email' => 'dietrich@agentur-webfox.de',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '0.0.3',
	'constraints' => array(
		'depends' => array(
			'typo3' => '7.6.0-8.7.99',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);
