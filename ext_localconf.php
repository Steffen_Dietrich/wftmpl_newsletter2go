<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
		'Webfox.' . $_EXTKEY,
		'List',
		array('Newsletter2GoAPI' => 'list, new')
);
